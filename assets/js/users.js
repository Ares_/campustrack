$(document).ready(function () {
	$("button#header-tooltip").click(function () { // Dark Theme Switch Handler
		if (Cookies.get('darkTheme') == 0) {
			Cookies.set('darkTheme', 1, {expires: 7});
			$('body').addClass('c-dark-theme');
			$("iframe").attr('src', $("iframe").attr('src').replace('&theme=light', '&theme=dark'));
		} else {
			Cookies.set('darkTheme', 0, {expires: 7});
			$('body').removeClass('c-dark-theme');
			$("iframe").attr('src', $("iframe").attr('src').replace('&theme=dark', '&theme=light'));
		}
		$(this).find('svg').toggle();
		$(this).blur();
	});

	$("table#users").dataTable({
		"fixedHeader": true,
		"responsive": true,
		"pageLength": 10,
		"ajax": {
			url: "/users/users_data",
			type: 'GET'
		},
		"columnDefs": [
			{
				"targets": [2],
				"createdCell": function (td, cellData, rowData, row, col) {
					if (cellData == 1) {
						$(td).html('<span class="badge badge-success">Oui</span>')
					} else {
						$(td).html('<span class="badge badge-danger">Non</span>')
					}
				}
			}]
	});
});
