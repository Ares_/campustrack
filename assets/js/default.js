$(document).ready(function () {
	if (Cookies.get('darkTheme') === undefined) { // Set automatic Dark theme from Windows pref
		if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
			Cookies.set('darkTheme', 1, {expires: 7});
			$('body').addClass('c-dark-theme');
			$("#darkThemeButtonEnable").hide();
			$("iframe").attr('src', $("iframe").attr('src').replace('&theme=light', '&theme=dark'))
		} else {
			Cookies.set('darkTheme', 0, {expires: 7});
			$('body').removeClass('c-dark-theme');
			$("#darkThemeButtonDisable").hide();
			$("iframe").attr('src', $("iframe").attr('src').replace('&theme=dark', '&theme=light'));
		}
	} else {
		Cookies.get('darkTheme') == 0 ? $('body').removeClass('c-dark-theme') : $('body').addClass('c-dark-theme');
		Cookies.get('darkTheme') == 0 ? $("#darkThemeButtonDisable").hide() : $("#darkThemeButtonEnable").hide();
		if ($("iframe").attr('src')) {
			Cookies.get('darkTheme') == 0 ? $("iframe").attr('src', $("iframe").attr('src').replace('&theme=dark', '&theme=light')) : $("iframe").attr('src', $("iframe").attr('src').replace('&theme=light', '&theme=dark'));
		}
	}
});
