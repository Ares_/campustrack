$(document).ready(function () {
	$("button#header-tooltip").click(function () { // Dark Theme Switch Handler
		if (Cookies.get('darkTheme') == 0) {
			Cookies.set('darkTheme', 1, {expires: 7});
			$('body').addClass('c-dark-theme');
			$("iframe").attr('src', $("iframe").attr('src').replace('&theme=light', '&theme=dark'));
		} else {
			Cookies.set('darkTheme', 0, {expires: 7});
			$('body').removeClass('c-dark-theme');
			$("iframe").attr('src', $("iframe").attr('src').replace('&theme=dark', '&theme=light'));
		}
		$(this).find('svg').toggle();
		$(this).blur();
	});

	$('form#validateExo').submit(function (event) { // Update Token Pushbullet Handler
		event.preventDefault();
		let data = {};
		data.id = $("input#id").val();
		data.password = $("input#password").val();
		$.ajax({
			url: '/Exercices/validate/' + data.id,
			type: 'POST',
			dataType: "json",
			data: data,
			success: function (response, status) {
				toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"positionClass": "toast-top-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "500",
					"hideDuration": "1000",
					"timeOut": "3000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
				if (response.status == "success") {
					$("input#password").addClass('is-valid').removeClass('is-invalid').prop('disabled', true);
					$('button[type="submit"]').prop('disabled', true).hide();
				} else {
					$("input#password").addClass('is-invalid').removeClass('is-valid');
				}
				toastr[response.status](response.info)
			},
			error: function (result, status, error) {
				console.log(result);
				console.log(status);
				console.log(error);
			},
		});
	});
	if ($("table#users").length == 1) {
		$("table#users").dataTable({
			"fixedHeader": true,
			"responsive": true,
			"pageLength": 5,
			"ajax": {
				url: "/users/user_data",
				type: 'GET'
			},
			"columnDefs": [
				{
					"targets": [1],
					"createdCell": function (td, cellData, rowData, row, col) {
						if (cellData == 1) {
							$(td).html('<span class="badge badge-success">Oui</span>')
						} else {
							$(td).html('<span class="badge badge-danger">Non</span>')
						}
					}
				}]
		});
	}

});
