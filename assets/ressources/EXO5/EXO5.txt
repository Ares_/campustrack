Exercice 5 : Le père d’SSH
Bonjour, 
On sait qu’un administrateur utilise un vieux protocole de communication pour prendre la main sur ces serveurs, ce protocole semble ne pas être sécurisé et par conséquent nous vous demandons de retrouver le mot de passe qui se cache surement dans la ressource fournie.

Ressource : exo5.pcap

Entre le mot de passe dans le champ suivant pour valider le challenge. Prends une capture d'écran en te connectant au serveur suivant avec les informations que tu trouvera dans le fichier


IP du serveur pour la capture d'écran : 51.222.121.166:44422


