<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="c-body">
	<main class="c-main">
		<div class="container-fluid">
			<div class="fade-in"></div>
			<div class="row">
				<div class="col-sm-12 col-md-12 col-xl-12 col-lg-12">
					<form class="form-horizontal" action="/Exercices/validate" method="post" id="validateExo">
						<input name="id" id="id" type="hidden" value="<?= $exerciceId ?>">
						<div class="card">
							<div class="card-header"><strong><?= $exerciceName ?></strong>
							</div>
							<div class="card-body">
								<p class="card-text">
									<?php
									if ($exerciceId == 3) {
										echo(str_replace("\r\n", '<br>', $exerciceDescription));
									} else {
										echo $exerciceDescription;
									}
									?>
								</p>
							</div>
							<div class="card-footer">
								<div class="row">
									<input class="form-control col-md-8 <?= $exerciceComplete ? 'is-valid' : '' ?>"
										   id="password" required type="text"
										   name="password" <?= $exerciceComplete ? 'disabled' : '' ?>
										   placeholder="Entrer le mot de passe"/>
									<?= !$exerciceComplete ? '<button class="btn btn-xs btn-primary offset-md-1 col-md-3" type="submit">
										Envoyer
									</button>' : '' ?>
								</div>
							</div>
						</div>
					</form>
				</div>
				<?php if (!empty($exerciceRessource)) {
					echo '<div class="col-sm-12 col-md-12 col-xl-12 col-lg-12">
						<div class="card">
							<div class="card-header"><strong>Ressource</strong>
							</div>
							<div class="card-body">
								Un fichier est disponible pour cet exercice.
							</div>
							<div class="card-footer">
								<div class="row">
									<form method="get" target="_blank" action="' . base_url() . $exerciceRessource . '">
									   <button class="btn btn-xs btn-primary" type="submit">Télécharger !</button>
									</form>
								</div>
							</div>
						</div>
				</div>';
				}
				?>
			</div>
		</div>
	</main>
</div>
<footer class="c-footer">
	<div class="row">
		<a href="https://track49-ops01.duckdns.org/">Root Track</a>&nbsp; &copy; 2021 &nbsp;<img
				height="21px" src="<?php echo base_url() . 'assets/img/LUNES_LOGO_favicon.svg' ?>">
	</div>
	<div class="ml-auto">Developed by<a href="https://track49-ops01.duckdns.org"> Angers Task force 1</a></div>
</footer>
</div>
</div>

