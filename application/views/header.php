<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.2.0
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>
	<base href="./">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="description" content="Root Track">
	<meta name="author" content="Guillaume Duarte">
	<meta name="keyword" content="Root Track">
	<title>Root Track</title>
	<link rel="icon" type="image/png" sizes="32x32"
		  href="<?php echo base_url() . 'assets/img/LUNES_LOGO_favicon.svg' ?>">

	<meta name="theme-color" content="#ffffff">
	<!-- Main styles for this application-->
	<link href="<?php echo base_url() . 'assets/css/style.css' ?>" rel="stylesheet">
	<link href="https://coreui.io/demo/3.4.0/vendors/toastr/css/toastr.min.css" rel="stylesheet">

	<!-- CoreUI and necessary plugins-->
	<script
			src="https://code.jquery.com/jquery-3.5.1.min.js"
			integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			crossorigin="anonymous"></script>

</head>
<body class="c-app">
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show c-sidebar-unfoldable" id="sidebar">
	<div class="c-sidebar-brand d-md-down-none">
		<div class="c-sidebar-brand-full" width="118" height="46">
			Root Track
		</div>
		<div class="c-sidebar-brand-minimized" width="46" height="46">
			<svg class="c-icon c-icon-lg">
				<use xlink:href="#cil-menu"></use>
			</svg>
		</div>
	</div>
	<ul class="c-sidebar-nav ps ps--active-y">
		<li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="home">
				<svg class="c-sidebar-nav-icon">
					<use xlink:href="https://unpkg.com/@coreui/icons/svg/free.svg#cil-speedometer"></use>
				</svg>
				Dashboard<span class="badge badge-info">NEW</span></a></li>
		<?php
		foreach (getExercices() as $currentExo) {
			echo '<li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/Exercices/numero/' . $currentExo->id . '">
					' . $currentExo->name . '</a>
					</li>';
		}
		?>

		<li class="c-sidebar-nav-divider"></li>

		<div class="ps__rail-x" style="left: 0px; bottom: 0px;">
			<div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
		</div>
		<div class="ps__rail-y" style="top: 0px; height: 831px; right: 0px;">
			<div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 532px;"></div>
		</div>
	</ul>
	<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
			data-class="c-sidebar-unfoldable"></button>
</div>

<div class="c-wrapper c-fixed-components">
	<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
		<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
				data-class="c-sidebar-show">
			<svg class="c-icon c-icon-lg">
				<use xlink:href="https://unpkg.com/@coreui/icons/sprites/free.svg#cil-menu"></use>
			</svg>
		</button>
		<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
				data-class="c-sidebar-lg-show" responsive="true">
			<svg class="c-icon c-icon-lg">
				<use xlink:href="https://unpkg.com/@coreui/icons/sprites/free.svg#cil-menu"></use>
			</svg>
		</button>
		<ul class="c-header-nav d-md-down-none">
			<li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Dashboard</a></li>
			<?php
			if ($this->session->admin) echo '
			<li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="/users">Users</a></li>
			';
			?>
		</ul>
		<ul class="c-header-nav ml-auto mr-4">
			<li class="c-header-nav-item px-3 c-d-legacy-none">
				<button class="c-header-nav-btn" type="button" id="header-tooltip"
						data-toggle="tooltip" data-placement="bottom"
						data-original-title="Toggle Light/Dark Mode">
					<svg class="c-icon" id="darkThemeButtonEnable">
						<use xlink:href="https://unpkg.com/@coreui/icons/sprites/free.svg#cil-moon"></use>
					</svg>
					<svg class="c-icon" id="darkThemeButtonDisable">
						<use xlink:href="https://unpkg.com/@coreui/icons/sprites/free.svg#cil-sun"></use>
					</svg>
				</button>
			</li>
			<li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#"
													  role="button" aria-haspopup="true" aria-expanded="false">
					<div class="c-avatar">
						<svg class="mr-2 c-avatar-img" style="fill: currentColor;">
							<use xlink:href="<?php echo base_url() . 'assets/svg/person-fill.svg' ?>#bi-person-fill"></use>
						</svg>
					</div>
				</a>
				<div class="dropdown-menu dropdown-menu-right pt-0">
					<div class="dropdown-header bg-light py-2"><strong>Account</strong></div>
					<a class="dropdown-item" href="logout">
						<svg class="c-icon mr-2">
							<use xlink:href="<?php echo base_url() . 'assets/svg/free.svg' ?>#cil-account-logout"></use>
						</svg>
						Déconnexion
					</a>
				</div>
			</li>
		</ul>
		<div class="c-subheader px-3">
			<!-- Breadcrumb-->
			<ol class="breadcrumb border-0 m-0">
				<?php
				if (isset($BREADCRUMBS)) {
					foreach ($BREADCRUMBS as $BREADCRUMB) {
						echo '
						<li class="breadcrumb-item ' . (next($BREADCRUMBS) != true ? "active" : "") . '">' . $BREADCRUMB . '</li>
						';
					}
				}
				?>
				<!-- Breadcrumb Menu-->
			</ol>
		</div>
	</header>
