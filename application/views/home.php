<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="c-body">
	<main class="c-main">
		<div class="container-fluid">
			<div class="fade-in"></div>
			<div class="row">
				<div class="col-sm-12 col-md-12 col-xl-12 col-lg-12">
					<div class="card">
						<div class="card-header">Listes des exercices</div>
						<div class="card-body">
							<div class="col-md-12">
								<table id="users" class="table table-bordered table-striped table-hover datatable">
									<thead>
									<tr>
										<th>Exercice</th>
										<th>Validé</th>
										<th>Date</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<footer class="c-footer">
	<div class="row">
		<a href="https://track49-ops01.duckdns.org/">Root Track</a>&nbsp; &copy; 2021 &nbsp;<img
				height="21px" src="<?php echo base_url() . 'assets/img/LUNES_LOGO_favicon.svg' ?>">
	</div>
	<div class="ml-auto">Developed by<a href="https://track49-ops01.duckdns.org"> Angers Task force 1</a></div>
</footer>
</div>
</div>

<link href="<?php echo base_url() . 'assets/css/dataTables.bootstrap4.min.css' ?>" rel="stylesheet">
<link href="<?php echo base_url() . 'assets/css/responsive.bootstrap4.min.css' ?>" rel="stylesheet">
<link href="<?php echo base_url() . 'assets/css/fixedHeader.dataTables.min.css' ?>" rel="stylesheet">

<script src="<?php echo base_url() . 'assets/js/jquery.dataTables.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/dataTables.bootstrap4.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/fixedHeader.bootstrap4.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/dataTables.responsive.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/responsive.bootstrap4.min.js' ?>"></script>
