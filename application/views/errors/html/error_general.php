<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.2.0
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>
	<base href="./">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="description" content="Root Track">
	<meta name="author" content="Guillaume Duarte">
	<meta name="keyword" content="Root Track">
	<title>Root Track</title>
	<link rel="icon" type="image/png" sizes="32x32"
		  href="<?php echo base_url() . 'assets/img/LUNES_LOGO_favicon.svg' ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- Main styles for this application-->
	<link href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet">
	<!-- Global site tag (gtag.js) - Google Analytics-->
	<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		// Shared ID
		gtag('config', 'UA-118965717-3');
		// Bootstrap ID
		gtag('config', 'UA-118965717-5');
	</script>
</head>
<body class="c-app flex-row align-items-center">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="clearfix">
				<h1 class="float-left display-3 mr-4">404</h1>
				<h4 class="pt-3">	<?php echo $heading; ?></h4>
				<p class="text-muted"><?php echo $message; ?></p>
			</div>
			<div class="input-prepend input-group justify-content-center">
				<a href="/"><button class="btn btn-info" type="button">Get back to Home</button></a></span>
			</div>
		</div>
	</div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="https://unpkg.com/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]-->
</body>
</html>


