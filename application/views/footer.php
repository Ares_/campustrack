<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>

<script src="https://unpkg.com/@coreui/coreui-pro/dist/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="https://unpkg.com/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]-->
<script src="https://coreui.io/demo/3.4.0/vendors/toastr/js/toastr.js"></script>

<script src="<?php echo base_url() . 'assets/js/tooltips.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/default.js' ?>"></script>
<?php
if (isset($js)) {
	foreach ($js as $currentJs) {
		echo "<script src=" . base_url() . "assets/js/" . $currentJs . "></script>";
	}
}
?>

</body>
</html>
