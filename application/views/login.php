<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.2.0
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>
	<base href="./">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="description" content="Root Track">
	<meta name="author" content="Guillaume Duarte">
	<meta name="keyword" content="Root Track">
	<title>Root Track</title>
	<link rel="icon" type="image/png" sizes="32x32"
		  href="<?php echo base_url() . 'assets/img/LUNES_LOGO_favicon.svg' ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	<!-- Main styles for this application-->
	<link href="<?php echo base_url() . 'assets/css/style.css' ?>" rel="stylesheet">
	<!-- Global site tag (gtag.js) - Google Analytics-->
</head>
<body class="c-app flex-row align-items-center">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card-group">
				<div class="card p-4">
					<div class="card-body">
						<form action="/login" method="post">
							<h1>Login</h1>
							<p class="text-muted">Sign In to your account</p>
							<?php echo validation_errors('<div class="alert alert-danger" role="alert">', '</div>'); ?>
							<?php if (!empty($this->session->flashdata('error'))) {
								echo '<div class="alert alert-danger" role="alert">' . $this->session->flashdata('error') . '</div>';
							}
							?>
							<div class="input-group mb-3">
								<div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="https://unpkg.com/@coreui/icons/sprites/free.svg#cil-user"></use>
                      </svg></span></div>
								<input class="form-control" type="text" name="username" required placeholder="ANGERSTF03">
							</div>
							<div class="input-group mb-4">
								<div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="https://unpkg.com/@coreui/icons/sprites/free.svg#cil-lock-locked"></use>
                      </svg></span></div>
								<input class="form-control" type="password" name="password" required minlength="6"
									   placeholder="Password">
							</div>
							<div class="row">
								<div class="col-6">
									<button class="btn btn-primary px-4" type="submit">Login</button>
								</div>
								<div class="col-6 text-right">
									<button class="btn btn-link px-0" type="button">Forgot password?</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
