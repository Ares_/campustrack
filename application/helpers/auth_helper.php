<?php
function is_logged_in()
{
	// Get current CodeIgniter instance
	$CI =& get_instance();
	if ($CI->session->isConnected !== true) {
		return false;
	} else {
		return true;
	}
}

function getExercices()
{
	// Get current CodeIgniter instance
	$CI =& get_instance();
	return $CI->db->get('exercices')->result_object();
}

function is_admin()
{
	// Get current CodeIgniter instance
	$CI =& get_instance();

	if (!is_logged_in() || !$CI->session->admin) {
		return false;
	} else {
		return true;
	}
}

function only_if_connected()
{
	// Get current CodeIgniter instance
	$CI =& get_instance();
	if (!is_logged_in()) {
		redirect('/login');
	} else {
		return true;
	}
}

function disconnect_user()
{
	$CI =& get_instance();
	if ($CI->session->has_userdata('id') || $CI->session->isConnected === true) {
		$CI->session->isConnected = false;
		return session_destroy();
	} else {
		return true;
	}

}
