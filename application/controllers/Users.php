<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct()
	{
		Parent::__construct();
		$this->load->model("users_model");
	}

	public function index()
	{
		if (is_logged_in()) {
			$js['js'] = array("users.js");
			$this->load->view('header', array("BREADCRUMBS" => array('Home', 'Dashboard')));
			$this->load->view('users');
			$this->load->view('footer', $js);
		} else {
			redirect('login');
		}
	}

	public function user_data()
	{

		// Datatables Variables
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$users = $this->db
			->query('SELECT * FROM `exercices`
						LEFT JOIN exercicesUsersLink ON exercices.id = exercicesUsersLink.exercicelink
						AND exercicesUsersLink.userLink = ?', $this->session->id);

		$data = array();

		foreach ($users->result() as $r) {

			$data[] = array(
				$r->name,
				$r->complete ? $r->complete : false,
				$r->creation_date
			);
		}
		$output = array(
			"draw" => $draw,
			"recordsTotal" => $users->num_rows(),
			"recordsFiltered" => $users->num_rows(),
			"data" => $data
		);
		echo json_encode($output);
		exit();
	}

	public function users_data()
	{

		// Datatables Variables
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));


		$users = $this->db->query('SELECT * FROM exercices e cross join users u 
    									left join exercicesUsersLink eul on u.id = eul.userLink and eul.exercicelink = e.id
								');
		$data = array();

		foreach ($users->result() as $r) {

			$data[] = array(
				$r->username,
				$r->name,
				$r->complete ? $r->complete : false,
				$r->creation_date
			);
		}
		$output = array(
			"draw" => $draw,
			"recordsTotal" => $users->num_rows(),
			"recordsFiltered" => $users->num_rows(),
			"data" => $data
		);
		echo json_encode($output);
		exit();
	}
}
