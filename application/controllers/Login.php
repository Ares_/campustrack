<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function index()
	{
		if (!is_logged_in()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
			$this->form_validation->set_rules('username', 'Username', 'required|max_length[35]');

			if ($this->form_validation->run() == true) {
				$data['password'] = $this->input->post('password');
				$data['username'] = strtoupper($this->input->post('username'));
				$query = $this->db->where('username', $data['username'])->get('users');
				if ($query->num_rows() == 1) {
					$user = $query->row();
					if (password_verify($data['password'], $user->password)) { // mdp ok
						$user->isConnected = true;
						unset($user->password);
						$this->session->set_userdata((array)$user);
						redirect('home');
					} else { // mdp faux
						$this->session->set_flashdata('error', 'Bad password');
						$this->load->view('login');
						$this->load->view('footer');
					}
				} else { // mail n'existe pas
					$this->session->set_flashdata('error', 'This Username doesn\'t exist');
					$this->load->view('login');
					$this->load->view('footer');
				}
			} else {
				$this->load->view('login');
				$this->load->view('footer');
			}
		} else { // user deja connecté
			redirect('home');
		}
	}
}
