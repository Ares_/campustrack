<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exercices extends CI_Controller
{

	public function index()
	{
		redirect('home');
	}

	public function numero($id = null)
	{
		if (is_logged_in()) {
			if (!empty($id)) {
				$flag = false;
				foreach (getExercices() as $currentExo) {
					if ($currentExo->id == $id) {
						$flag = true;
						$data = array(
							'exerciceId' => $currentExo->id,
							'exerciceName' => $currentExo->name,
							'exerciceDescription' => $currentExo->description,
							'exerciceRessource' => $currentExo->ressource
						);
						$query = $this->db
							->where('userLink', $this->session->id)
							->where('exercicelink', $currentExo->id)
							->get('exercicesUsersLink');
						$data['exerciceComplete'] = $query->num_rows() >= 1 ? filter_var($query->row(0)->complete, FILTER_VALIDATE_BOOL) : false;
						$js['js'] = array("home.js");
						$this->load->view('header', array("BREADCRUMBS" => array('Home', 'exercice', $id)));
						$this->load->view('exercices', $data);
						$this->load->view('footer', $js);
						break;
					}
				}
				if (!$flag) {
					redirect('home');
				}
			} else {
				redirect('home');
			}
		} else {
			redirect('home');
		}
	}

	public function validate($id)
	{
		if (empty($id))
			$id = $this->input->post('id');
		$password = $this->input->post('password');
		$user = $this->session->id;
		if (!empty($id) && !empty($password) && !empty($user)) {
			$query = $this->db->where('id', intval($id))->where('password', $password)->get('exercices');
			if ($query->num_rows() == 1) {
				$data = array(
					'userLink' => $this->session->id,
					'exercicelink' => $id,
					'complete' => true
				);
				$query2 = $this->db
					->where('userLink', $this->session->id)
					->where('exercicelink', $id)
					->get('exercicesUsersLink');
				if ($query2->num_rows() == 0) {
					$res = $this->db->insert('exercicesUsersLink', $data);
					if ($res) {
						$response['status'] = "success";
						$response['info'] = "Le mot de passe est correcte !";
					} else {
						trigger_error('impossible d\'inserer le numéro users : ' . $id, E_USER_ERROR);
					}
				} else {
					$response['status'] = "error";
					$response['info'] = "Le mot de passe à déjà été validé !";
				}
			} else {
				$response['status'] = "error";
				$response['info'] = "Le mot de passe est incorrecte !";
			}
		} else {
			$response['status'] = "error";
			$response['info'] = "Une erreur c'est produite merci de vous reconnecter";
			//trigger_error('impossible d\'inserer le numéro users : ' . $id, E_USER_ERROR);
		}
		echo json_encode($response);
	}
}
