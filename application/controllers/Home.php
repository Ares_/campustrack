<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function index()
	{
		if (is_logged_in()) {
			$js['js'] = array("home.js");
			$this->load->view('header', array("BREADCRUMBS" => array('Home', 'Dashboard')));
			$this->load->view('home');
			$this->load->view('footer', $js);

		} else {
			redirect('login');
		}
	}
}
