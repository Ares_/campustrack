<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends CI_Controller
{

	public function index()
	{
		if (is_logged_in()) {
			disconnect_user();
			redirect('login');
		} else {
			redirect('login');
		}
	}
}
